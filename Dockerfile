FROM node:10.15

ENV HASH_SALT=s3cr3t \
    JWT_SECRET=verys3cr3t \
    MONGO_URL=mongodb://webuser:webuser@localhost:27017/dev

WORKDIR /app
COPY . /app
RUN npm i && \
    npm run build

EXPOSE 8000
CMD npm run serve
