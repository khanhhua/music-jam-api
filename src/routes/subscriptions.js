import { Router } from 'express';
import { v4 as uuidV4 } from 'uuid';
import Profile from '../models/Profile';
var router = Router();

router.post('/', async (req, res) => {
  const { sub } = req.user;
  const profile = await Profile.findById(sub);
  if (profile.subscriptionId) {
    return res.status(200).send({
      ok: true,
      subId: profile.subscriptionId,
    });
  }

  profile.subscriptionId = uuidV4();
  await profile.save();
  return res.status(200).send({
    ok: true,
    subId: profile.subscriptionId,
  });
});

export default router;
