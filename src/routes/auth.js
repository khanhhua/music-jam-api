import { Router } from 'express';
import jwt from 'jsonwebtoken';
import { hashPass } from '../utils';
import Profile from '../models/Profile';
var router = Router();

const HASH_SALT = process.env.HASH_SALT;
const JWT_SECRET = process.env.JWT_SECRET;

router.post('/', async (req, res) => {
  const { username, password } = req.body;
  const hashedPass = hashPass(password, HASH_SALT);
  const profile = await Profile.findOne({
    username,
    password: hashedPass,
  }).lean();

  if (!profile) {
    return res.status(403).send({
      ok: false,
      error: 'Access denied',
    });
  }

  const token = jwt.sign({ sub: profile._id }, JWT_SECRET, { expiresIn: '4h' });

  return res.status(200).send({
    ok: true,
    token,
    userId: profile._id.toString(),
  });
});

export default router;
