import { Router } from 'express';
import Song from '../models/Song';
var router = Router();

router.post('/', async (req, res) => {
  const { title, instruments } = req.body;
  if (!title || !instruments) {
    return res.status(400).send({
      ok: false,
      error: 'Bad request',
    });
  }

  const song = await Song.create({
    title,
    instruments,
  });

  return res.status(201).json({
    ok: true,
    song: song.toJSON(),
  })
});

router.get('/', async (req, res) => {
  const songs = await Song.find({});

  return res.json({
    ok: true,
    songs: songs.map(item => item.toJSON()),
  });
});

export default router;
