import { Router } from 'express';
import { hashPass } from '../utils';
import Profile from '../models/Profile';

const router = Router();
const HASH_SALT = process.env.HASH_SALT;

router.post('/', async (req, res) => {
  const { username, password, instrument } = req.body;
  if (!username || !password || !instrument) {
    return res.status(400).send({
      ok: false,
      error: 'Bad request',
    });
  }

  const profile = await Profile.create({ username, password: hashPass(password, HASH_SALT), instrument });
  const json = profile.toJSON();
  delete json.password;

  return res.json({
    ok: true,
    profile: json
  });
});

router.get('/me', async (req, res) => {
  const { user } = req;
  const profile = await Profile.findById(user.sub);
  if (!profile) {
    return res.status(404).send({
      ok: false,
      error: 'Not found',
    });
  }

  return res.json({
    ok: true,
    profile: profile.toJSON(),
  })
});

router.patch('/me', async (req, res) => {
  const { user, body } = req;
  const { instrument } = body;
  const { nModified } = await Profile.updateOne({ _id: user.sub }, { $set: { instrument }});
  if (!nModified) {
    return res.status(404).send({
      ok: false,
      error: 'Not found',
    });
  }

  return res.json({
    ok: true,
    profile: (await Profile.findById(user.sub)).toJSON(),
  })
});

export default router;
