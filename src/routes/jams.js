import { Router } from 'express';
import Song from '../models/Song';
import Jam from '../models/Jam';
import Profile from '../models/Profile';
import { pushMessage } from '../wss';
const router = Router();

/**
 * Broadcast jam update to jam parties
 *
 * @param jam
 * @returns {Promise<[]>}
 */
async function broadcastUpdate(jam) {
  const playerProfiles = await Profile.find({
    _id: {
      $in: jam.players,
    }
  });
  return Promise.all(playerProfiles.map(item => {
    if (item.subscriptionId) {
      return pushMessage(item.subscriptionId, {
        type: 'jam:updated',
        jam: {
          ...jam,
          playerProfiles: playerProfiles.map(player => player.toJSON()),
        },
      });
    }
    return Promise.resolve();
  }));
}

async function broadcastCreate(jam) {
  const allProfiles = await Profile.find({});
  return Promise.all(allProfiles.map(item => {
    if (item.subscriptionId) {
      return pushMessage(item.subscriptionId, {
        type: 'jam:created',
        jam,
      });
    }
    return Promise.resolve();
  }));
}

router.post('/', async (req, res) => {
  const { sub } = req.user;
  const { songId } = req.body;
  if (!songId) {
    return res.status(400).json({
      ok: false,
      error: 'Bad request',
    });
  }

  const song = await Song.findById(songId);
  if (!song) {
    return res.status(400).json({
      ok: false,
      error: 'Not found',
    });
  }
  const profile = await Profile.findById(sub);

  const jam = await Jam.create({
    title: song.title,
    songId: song._id,
    ownerId: sub,
    instrumentsRequired: song.instruments,
    instrumentsMet: [profile.instrument],
    players: [profile._id],
  });

  const json = jam.toJSON();
  res.status(201).json({
    ok: true,
    jam: json,
  });

  await broadcastCreate(json);
  return;
});

router.get('/:me((me)?)', async (req, res) => {
  const { limit = 20, status = 'pending,ready,inprog' } = req.query;
  const { me } = req.params;
  const condition = me ? { ownerId: req.user.sub } : {};
  condition.status = {
    $in: status.split(',').map(item => item.toUpperCase())
  };
  const jams = await Jam.find(condition).limit(limit);

  return res.status(200).json({
    ok: true,
    jams: jams.map(item => item.toJSON()),
  });
});

router.get('/:id', async (req, res) => {
  const { id } = req.params;
  const jam = await Jam.findById(id);

  if (!jam) {
    return res.status(404).json({
      ok: false,
      error: 'Not found',
    });
  }

  const playerProfiles = await Profile.find({
    _id: { $in: jam.players }
  }, {
    username: 1,
    instrument: 1,
  }).lean();
  const json = {
    ...jam.toJSON(),
    playerProfiles,
  };

  return res.status(200).json({
    ok: true,
    jam: json,
  });
});

router.post('/:id/players', async (req, res) => {
  const { sub } = req.user;
  const { id } = req.params;

  const jam = await Jam.findById(id);
  if (jam.ownerId && jam.ownerId.toString() === sub) {
    return res.status(400).json({
      ok: false,
      error: 'Bad request',
    });
  }

  const profile = await Profile.findById(sub);
  // Transactional update??
  await Jam.updateOne({ _id: id }, {
    $addToSet: {
      players: sub,
      instrumentsMet: profile.instrument,
    },
  });

  const updatedJam = await Jam.findById(id).lean();
  if (updatedJam.instrumentsRequired.every(item => updatedJam.instrumentsMet.includes(item))) {
    await Jam.updateOne({ _id: id }, {
      status: 'READY',
    });
  }

  const json = (await Jam.findById(id).lean());
  res.status(200).json({
    ok: true,
    jam: json,
  });

  await broadcastUpdate(json);
  return;
});

router.post('/:id/sessions', async (req, res) => {
  const { sub } = req.user;
  const { id } = req.params;

  const jam = await Jam.findById(id);
  if (jam.ownerId && jam.ownerId.toString() !== sub) {
    return res.status(403).json({
      ok: false,
      error: 'Access denied',
    });
  }

  jam.status = 'INPROG';
  await jam.save();

  const json = jam.toJSON();
  res.status(200).send({
    ok: true,
    jam: json,
  });

  await broadcastUpdate(json);
});

router.patch('/:id', async (req, res) => {
  const { id } = req.params;
  const { status } = req.body;
  const jam = await Jam.findById(id);

  if (!jam) {
    return res.status(404).json({
      ok: false,
      error: 'Not found',
    });
  }

  if (!jam.ownerId) {
    return res.status(500).json({
      ok: false,
      error: 'Internal data error',
    });
  }

  if (jam.ownerId.toString() !== req.user.sub) {
    return res.status(403).json({
      ok: false,
      error: 'Access denied',
    });
  }

  const song = await Song.findById(jam.songId, { title: 1 }).lean();
  if (!song) {
    return res.status(404).json({
      ok: false,
      error: 'Not found',
    });
  }

  const updatedJam = await Jam.findOneAndUpdate({ _id: id },
      { status },
      { new: true });

  return res.status(200).json({
    ok: true,
    jam: updatedJam.toJSON(),
  });
});

export default router;
