import url from 'url';
import WebSocket from 'ws';
import DEBUG from 'debug';
import Profile from './models/Profile';
import Jam from './models/Jam';

const debug = DEBUG('music-jam:wss');
const wss = new WebSocket.Server({ noServer: true });

const registry = {};
const rgx = /\/ws\/([0-9a-f]{8}\b-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-\b[0-9a-f]{12})/;

export const pushMessage = async (subscriptionId, msg) => {
  if (!(subscriptionId in registry)) {
    return;
  }
  const payload = JSON.stringify(msg);
  for (const ws of registry[subscriptionId]) {
    ws.send(payload);
  }

  return true;
};

wss.on('connection', async (ws, req) => {
  debug('Websocket inbound...');

  const pathname = url.parse(req.url).pathname;
  const match = rgx.exec(pathname);

  if (!match) {
    debug('Dropping websocket connection. Reason: invalid subscription ID');
    ws.terminate();
    return;
  }

  const subId = match[1];
  const profileExists = await Profile.exists({ subscriptionId: subId });
  if (!profileExists) {
    debug('Dropping websocket connection. Reason: subscription ID registered in DB');
    ws.terminate();
    return;
  }

  ws.send(`hello: ${subId}`);
  // TODO Validate subscription ID
  // TODO Link req to ws
  ws.subId = subId;
  if (subId in registry) {
    registry[subId].push(ws);
  } else {
    registry[subId] = [ws];
  }

  ws.on('message', (msg) => {
    debug(`[${subId}] Receiving message...`);
    ws.send('pong');
  });

  ws.on('close', () => {
    if (ws.subId in registry) {
      registry[ws.subId] = registry[ws.subId].filter(item => item !== ws);
    }
  })
});

export default wss;
