import { model, Schema } from 'mongoose';

const Jam = model('Jam', new Schema({
  status: {
    type: String,
    enum: ['PENDING', 'READY', 'INPROG', 'DONE'], // pending: waiting for players, ready: all instruments are met, inprog: recording, jamming, done: jam is good to go!
    default: 'PENDING',
  },
  songId: {
    type: Schema.Types.ObjectId,
    ref: 'Song',
  },
  title: String,
  ownerId: {
    type: Schema.Types.ObjectId,
    ref: 'Profile',
  },
  players: [{
    type: Schema.Types.ObjectId,
    ref: 'Profile',
  }],
  instrumentsRequired: [String], // copied from song.instruments
  instrumentsMet: [String], // copied from player
}));

export default Jam;
