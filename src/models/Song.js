import { model, Schema } from 'mongoose';

const Song = model('Song', new Schema({
  title: String,
  instruments: [String],
}));

export default Song;
