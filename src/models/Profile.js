import { model, Schema } from 'mongoose';

const Profile = model('Profile', new Schema({
  username: String,
  password: String,
  instrument: String,
  subscriptionId: String,
}, {
  toJSON: {
    transform(doc, ret, options) {
      delete ret.password;
      delete ret.subscriptionId;
      return ret;
    }
  }
}));

export default Profile;
