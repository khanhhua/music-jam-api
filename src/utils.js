import crypto from 'crypto';

export const hashPass = (password, salt) => {
  return crypto.createHash('SHA1').update(`${password}:${salt}`).digest().toString('base64');
};
