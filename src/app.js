import express from 'express';
import path from 'path';
import logger from 'morgan';
import jwt from 'express-jwt';
import cors from 'cors';

import authRouter from './routes/auth';
import profilesRouter from './routes/profiles';
import jamsRouter from './routes/jams';
import songsRouter from './routes/songs';
import subscriptionsRouter from './routes/subscriptions';

const JWT_SECRET = process.env.JWT_SECRET;
const app = express();

app.use(logger('dev'));
app.use(cors({
  origin: true,
  methods: ['GET', 'POST', 'PUT', 'PATCH', 'DELETE'],
}));

app.use('/api', [
  jwt({ secret: JWT_SECRET })
    .unless({
      path: ['/ws', '/api/auth', '/api/profiles'],
      method: 'OPTIONS'
    }),
  (err, req, res, next) => {
    if (res.headersSent) {
      return next(err);
    }

    if (err.name === 'UnauthorizedError') {
      return res.status(401).json({
        ok: false,
        error: err.message,
      });
    }

    next(err);
  }]);
app.use(express.json());
app.use(express.urlencoded({ extended: false }));

app.use('/api/auth', authRouter);
app.use('/api/profiles', profilesRouter);
app.use('/api/jams', jamsRouter);
app.use('/api/songs', songsRouter);
app.use('/api/subscriptions', subscriptionsRouter);

export default app;
