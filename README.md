This project was bootstrapped with WebStorm NodeJS Express Template

## Before you start

- Copy `.env.example` to `.env`
- For development, fire up `docker-compose run mongo`
- For local test runs, fire up `docker-compose run mongo_test`

## Available Scripts

In the project directory, you can run:

### `npm run db:seed`

Populate database with popular songs that I like. Sorry if the collection does not match your expectation :D.

### `npm start`

Runs the app in the development mode.

For auto-reload, use `npm run start:dev`

### `npm run start:dev`

Runs the app with auto-reload (with `nodemon`)

### `npm test`

Runs tests found under `test` folder. **Be sure to fire up mongo_test container first!**

### `npm run build`

Builds the app for production to the `dist` folder.<br />
Your app is ready to be deployed!

### `npm run coverage`

Run all test with code coverage

### `npm run build`

Transpile code JavaScript to ES5 compat version under folder `dist`

### `npm run serve`

Runs transpiled version of the project

## Deployment

You may build a docker image and deploy on any platform (AWS ECS, DigitalOcean).

Be sure to provide the following __environment variables__:

### HASH_SALT

Salt added to password before SHA1 hashing to provide more entropy (enhance security).

### JWT_SECRET

Encryption key for JWT

### MONGO_URL 

MongoDB connection string. Yes, this project uses MongoDB as storage backend.
```dotenv
mongodb://webuser:webuser@localhost:27018/test
```
