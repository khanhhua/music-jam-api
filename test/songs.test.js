import { expect } from 'chai';
import Profile from '../src/models/Profile';
import { hashPass } from '../src/utils';
import request from 'supertest';
import app from '../src/app';
import mongoose from 'mongoose';
import Song from '../src/models/Song';

const MONGO_URL = process.env.MONGO_URL;
const HASH_SALT = process.env.HASH_SALT;

describe('Song API', () => {
  let token;

  after(async () => {
    await mongoose.connection.db.dropCollection('profiles');
    await mongoose.connection.db.dropCollection('songs');
    await mongoose.connection.close();
  });

  before(async () => {
    await mongoose.connect(MONGO_URL,{
      useNewUrlParser: true,
      useUnifiedTopology: true
    });

    await Song.collection.insertMany([
      {
        title: 'My Heart Will Go on',
        instruments: ['drum', 'guitar']
      },
      {
        title: 'Hit me baby one more time',
        instruments: ['piano', 'violin']
      },
      {
        title: 'Get down!',
        instruments: ['drum', 'saxophone']
      }
    ]);

    await Profile.create({
      username: 'tom',
      password: hashPass('jerry', HASH_SALT),
      instrument: 'piano',
    });

    const { body } = await request(app)
        .post('/api/auth')
        .send({
          username: 'tom',
          password: 'jerry',
        })
        .expect('Content-Type', /json/)
        .expect(200);
    expect(body).to.have.all.keys('ok', 'token', 'userId');
    token = body.token;
  });

  describe('POST /api/songs', () => {
    it('should create a new song', async () => {
      const { body } = await request(app)
          .post('/api/songs')
          .set('Authorization', `Bearer ${token}`)
          .send({
            title: 'Endless song',
            instruments: ['piano', 'violin']
          })
          .expect(201)
          .expect('Content-Type', /json/);
      expect(body).to.have.all.keys('ok', 'song');
      expect(body.ok).to.be.true;
      expect(body.song).to.deep.include({
        title: 'Endless song',
        instruments: ['piano', 'violin'],
      });
    });
  });

  describe('GET /songs', () => {
    it('should get my song', async () => {
      const { body } = await request(app)
          .get('/api/songs')
          .set('Authorization', `Bearer ${token}`)
          .expect(200)
          .expect('Content-Type', /json/);
      expect(body).to.have.all.keys('ok', 'songs');
      expect(body.ok).to.be.true;
      expect(body.songs).to.be.an('array');
    });
  });

  describe('GET /songs/:id', () => {
    it('should get my song', () => {
      expect(true).to.be.true;
    });
  });

  describe('PATCH /songs/:id', () => {
    it('should partially update a song', () => {
      expect(true).to.be.true;
    });
  });
});
