import { expect } from 'chai';
import mongoose from 'mongoose';
import Song from '../src/models/Song';
import Profile from '../src/models/Profile';
import { hashPass } from '../src/utils';
import request from 'supertest';
import app from '../src/app';
import Jam from '../src/models/Jam';
import { __RewireAPI__ } from '../src/routes/jams';

const MONGO_URL = process.env.MONGO_URL;
const HASH_SALT = process.env.HASH_SALT;

describe('Jam API', () => {
  let token;

  before(() => {
    __RewireAPI__.__Rewire__('pushMessage', () => Promise.resolve())
  });

  before(async () => mongoose.connect(MONGO_URL,{
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useFindAndModify: false,
  }));

  after(async () => {
    await mongoose.connection.close();
  });

  afterEach(async () => {
    await mongoose.connection.db.dropCollection('profiles');
    await mongoose.connection.db.dropCollection('songs');
    await mongoose.connection.db.dropCollection('jams');
  });

  beforeEach(async () => {
    await Song.collection.insertMany([
      {
        title: 'My Heart Will Go on',
        instruments: ['drum', 'guitar']
      },
      {
        title: 'Hit me baby one more time',
        instruments: ['piano', 'violin']
      },
      {
        title: 'Get down!',
        instruments: ['drum', 'saxophone']
      }
    ]);
    const profile = await Profile.create({
      username: 'tom',
      password: hashPass('jerry', HASH_SALT),
      instrument: 'piano',
    });
    const anonymous = await Profile.create({
      username: 'anonymous',
      password: hashPass('jerry', HASH_SALT),
      instrument: 'piano',
    });

    const song02 = await Song.findOne({ title: 'Hit me baby one more time' });
    const song03 = await Song.findOne({ title: 'Get down!' });
    await Jam.collection.insertMany([
      {
        ownerId: profile._id,
        songId: song02._id,
        title: song02.title,
        instrumentsRequired: song02.instruments,
        status: 'PENDING',
      },
      {
        ownerId: profile._id,
        songId: song03._id,
        title: song03.title,
        instrumentsRequired: song03.instruments,
        status: 'PENDING',
      },
      {
        ownerId: anonymous._id,
        songId: song03._id,
        title: song03.title,
        instrumentsRequired: song03.instruments,
        status: 'PENDING',
      }
    ]);

    const { body } = await request(app)
        .post('/api/auth')
        .send({
          username: 'tom',
          password: 'jerry',
        })
        .expect('Content-Type', /json/)
        .expect(200);
    expect(body).to.have.all.keys('ok', 'token', 'userId');
    token = body.token;
  });

  describe('POST /api/jams', () => {
    it('should create a new jam', async () => {
      const song = await Song.findOne({ title: 'My Heart Will Go on' });
      const owningProfile = await Profile.findOne({ username: 'tom' });

      const { body } = await request(app)
          .post('/api/jams')
          .set('Authorization', `Bearer ${token}`)
          .send({
            songId: song._id,
          })
          .expect(201)
          .expect('Content-Type', /json/);
      expect(body).to.have.all.keys('ok', 'jam');
      expect(body.ok).to.be.true;
      expect(body.jam).to.deep.include({
        title: song.title,
        ownerId: owningProfile._id.toString(),
        songId: song._id.toString(),
        instrumentsRequired: song.instruments,
        instrumentsMet: ['piano'],
      });
    });
  });

  describe('GET /jams', () => {
    it('should get public jams', async () => {
      const { body } = await request(app)
          .get('/api/jams')
          .set('Authorization', `Bearer ${token}`)
          .expect(200)
          .expect('Content-Type', /json/);
      expect(body).to.have.all.keys('ok', 'jams');
      expect(body.ok).to.be.true;
      expect(body.jams).to.be.an('array').that.has.length(3);
    });
  });

  describe('GET /jams/me', () => {
    it('should get my active (pending, open) jams', async () => {
      const profile = await Profile.findOne({ username: 'tom' });
      const { body } = await request(app)
          .get('/api/jams/me')
          .set('Authorization', `Bearer ${token}`)
          .expect(200)
          .expect('Content-Type', /json/);
      expect(body).to.have.all.keys('ok', 'jams');
      expect(body.ok).to.be.true;
      expect(body.jams).to.be.an('array').that.has.length(2);
      body.jams.forEach(item =>
          expect(item).to.have.all.keys(
              '_id', 'title', 'instrumentsRequired', 'instrumentsMet', 'status',
              'ownerId', 'players', 'songId')
      );
      expect(body.jams.map(({ ownerId }) => ownerId)).to.be.deep.equal([
          profile._id.toString(),
          profile._id.toString(),
      ]);
    });
  });

  describe('GET /jams/:id', () => {
    it('should get a jam by ID', async () => {
      const owningProfile = await Profile.findOne({ username: 'tom' });
      const jam = await Jam.findOne();
      const song = await Song.findById(jam.songId);
      const { body } = await request(app)
          .get(`/api/jams/${jam._id.toString()}`)
          .set('Authorization', `Bearer ${token}`)
          .expect(200)
          .expect('Content-Type', /json/);
      expect(body).to.have.all.keys('ok', 'jam');
      expect(body.ok).to.be.true;
      expect(body.jam).to.deep.include({
        title: song.title,
        ownerId: owningProfile._id.toString(),
        songId: song._id.toString(),
        instrumentsRequired: song.instruments,
        instrumentsMet: [],
      });
    });
  });

  describe('PATCH /jams/:id', () => {
    it('should partially update my own jam', async () => {
      const owningProfile = await Profile.findOne({ username: 'tom' });
      const jamA = await Jam.findOne({ ownerId: owningProfile._id });
      const { body } = await request(app)
          .patch(`/api/jams/${jamA._id.toString()}`)
          .set('Authorization', `Bearer ${token}`)
          .send({
            status: 'OPEN',
          })
          .expect(200)
          .expect('Content-Type', /json/);
      expect(body).to.have.all.keys('ok', 'jam');
      expect(body.ok).to.be.true;
      expect(body.jam).to.deep.include({
        ...jamA.toJSON(),
        status: 'OPEN',
        _id: jamA._id.toString(),
        ownerId: jamA.ownerId.toString(),
        songId: jamA.songId.toString(),
      });
    });

    it('should not update a jam that does not belong to me', async () => {
      const owningProfile = await Profile.findOne({username: 'anonymous'});
      // owningProfile and token owner are different now
      const jamA = await Jam.findOne({ownerId: owningProfile._id});
      await request(app)
          .patch(`/api/jams/${jamA._id.toString()}`)
          .set('Authorization', `Bearer ${token}`)
          .send({
            status: 'OPEN',
          })
          .expect(403)
          .expect('Content-Type', /json/);
    });
  });

  describe('POST /jams/:id/players', () => {
    it('should add myself into player list', async () => {
      const profile = await Profile.findOne({ username: 'tom' });
      const jam = await Jam.findOne({
        ownerId: { $ne: profile._id },
      });
      const { body } = await request(app)
          .post(`/api/jams/${jam._id.toString()}/players`)
          .set('Authorization', `Bearer ${token}`)
          .expect(200)
          .expect('Content-Type', /json/);
      expect(body).to.have.all.keys('ok', 'jam');
      expect(body.ok).to.be.true;
      expect(body.jam).to.deep.include({
        players: [profile._id.toString()]
      });
    });

    it('should add myself into my own jam\'s list', async () => {
      const profile = await Profile.findOne({ username: 'tom' });
      const jam = await Jam.findOne({
        ownerId: profile._id,
      });
      await request(app)
          .post(`/api/jams/${jam._id.toString()}/players`)
          .set('Authorization', `Bearer ${token}`)
          .expect(400)
          .expect('Content-Type', /json/);
    });
  });
});
