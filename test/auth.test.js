import { expect } from 'chai';

import mongoose from 'mongoose';
import request from 'supertest';
import jwt from 'jsonwebtoken';

import app from '../src/app';
import Profile from '../src/models/Profile';
import { hashPass } from '../src/utils';

const HASH_SALT = process.env.HASH_SALT;
const JWT_SECRET = process.env.JWT_SECRET;
const MONGO_URL = process.env.MONGO_URL;

describe('Auth API', () => {
  beforeEach(async () => {
    await mongoose.connect(MONGO_URL,{
      useNewUrlParser: true,
      useUnifiedTopology: true
    });
    await Profile.create({
      username: 'tom',
      password: hashPass('jerry', HASH_SALT),
    });
  });

  after(async () => {
    await mongoose.connection.db.dropCollection('profiles');
    await mongoose.connection.close();
  });

  describe('POST /api/auth', () => {
    it('should create a new auth token', async () => {
      const { body } = await request(app)
          .post('/api/auth')
          .send({
            username: 'tom',
            password: 'jerry',
          })
          .expect('Content-Type', /json/)
          .expect(200);
      expect(body).to.have.all.keys('ok', 'token', 'userId');
      expect(body.ok).to.be.true;

      const { token, userId } = body;
      const decoded = jwt.verify(token, JWT_SECRET);
      expect(decoded.sub).to.be.equal(userId);
      expect(await Profile.findById(decoded.sub).lean()).to.include({ username: 'tom' });
    });

    it('should reject invalid login', async () => {
      const { body } = await request(app)
          .post('/api/auth')
          .send({
            username: 'tom',
            password: 'jerry1',
          })
          .expect('Content-Type', /json/)
          .expect(403);
      expect(body).to.have.all.keys('ok', 'error');
      expect(body.ok).to.be.false;
    });
  });
});
