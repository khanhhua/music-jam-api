import { expect } from 'chai';
import mongoose from 'mongoose';
import request from 'supertest';
import Profile from '../src/models/Profile';
import app from '../src/app';
import { hashPass } from '../src/utils';

const MONGO_URL = process.env.MONGO_URL;
const HASH_SALT = process.env.HASH_SALT;

describe('Profile API', () => {
  beforeEach(async () => {
    await mongoose.connect(MONGO_URL,{
      useNewUrlParser: true,
      useUnifiedTopology: true
    });
  });

  after(async () => {
    await mongoose.connection.db.dropCollection('profiles');
    await mongoose.connection.close();
  });

  describe('POST /api/profiles', () => {
    it('should create a new profile', async () => {
      const { body } = await request(app)
          .post('/api/profiles')
          .send({
            username: 'newbie',
            password: 'badpass',
            instrument: 'piano',
          })
          .expect(200)
          .expect('Content-Type', /json/);
      expect(body).to.have.all.keys('ok', 'profile');
      expect(body.ok).to.be.true;
      expect(body.profile).to.have.all.keys('username', 'instrument', '_id', '__v').but.not.have.keys('password');
      expect(body.profile).to.include({
        username: 'newbie',
        instrument: 'piano',
      });
    });
  });

  describe('My Profile', () => {
    let token;
    before(async () => {
      await Profile.create({
        username: 'tom',
        password: hashPass('jerry', HASH_SALT),
        instrument: 'piano',
      });

      const { body } = await request(app)
          .post('/api/auth')
          .send({
            username: 'tom',
            password: 'jerry',
          })
          .expect('Content-Type', /json/)
          .expect(200);
      expect(body).to.have.all.keys('ok', 'token', 'userId');
      token = body.token;
    });

    describe('GET /api/profiles/me', () => {
      it('should get my profile', async () => {
        const { body } = await request(app)
            .get('/api/profiles/me')
            .set('Authorization', `Bearer ${token}`)
            .expect(200)
            .expect('Content-Type', /json/);
        expect(body).to.have.all.keys('ok', 'profile');
        expect(body.ok).to.be.true;
        expect(body.profile).to.have.all.keys('username', 'instrument', '_id', '__v').but.not.have.keys('password');
        expect(body.profile).to.include({
          username: 'tom',
          instrument: 'piano',
        })
      });
    });

    describe('PATCH /profiles/me', () => {
      it('should partially update my profile', async () => {
        const { body } = await request(app)
            .patch('/api/profiles/me')
            .send({
              instrument: 'trumpet',
            })
            .set('Authorization', `Bearer ${token}`)
            .expect(200)
            .expect('Content-Type', /json/);
        expect(body).to.have.all.keys('ok', 'profile');
        expect(body.ok).to.be.true;
        expect(body.profile).to.have.all.keys('username', 'instrument', '_id', '__v').but.not.have.keys('password');
        expect(body.profile).to.include({
          username: 'tom',
          instrument: 'trumpet',
        })
      });
    });
  });
});
