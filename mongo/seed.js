import mongoose from 'mongoose';

const MONGO_URL = process.env.MONGO_URL;

(async () => {
  await mongoose.connect(MONGO_URL, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useFindAndModify: false,
  });

  await mongoose.connection.collection('songs').insertMany([
    {
      title: 'Baby one more time',
      instruments: ['piano', 'guitar'],
    },
    {
      title: 'Larger than life',
      instruments: ['drum', 'guitar'],
    },
    {
      title: 'Dancing Queen',
      instruments: ['drum', 'saxophone'],
    },
    {
      title: 'No Woman No Cry',
      instruments: ['piano', 'tambourine'],
    },
    {
      title: 'Bohemian Rhapsody',
      instruments: ['piano', 'violin'],
    },
    {
      title: 'Radio',
      instruments: ['drum', 'guitar'],
    },
    {
      title: 'Du hast',
      instruments: ['violin', 'guitar'],
    }
  ]);

  await mongoose.connection.close();
})();
